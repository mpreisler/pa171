#ifndef STEGANOGRAPHY_BIT_MANIPULATION_H_
#define STEGANOGRAPHY_BIT_MANIPULATION_H_

inline void setNthBit(int& value, int n, bool bit)
{
    if (bit)
        value |= 1 << n;
    else
        value &= ~(1 << n);
}

inline bool getNthBit(int value, int n)
{
    return (value >> n) & 1;
}

inline void setNthBit(char& value, int n, bool bit)
{
    if (bit)
        value |= 1 << n;
    else
        value &= ~(1 << n);
}

inline bool getNthBit(char value, int n)
{
    return (value >> n) & 1;
}

inline void setNthBit(unsigned short& value, int n, bool bit)
{
    if (bit)
        value |= 1 << n;
    else
        value &= ~(1 << n);
}

inline bool getNthBit(unsigned short value, int n)
{
    return (value >> n) & 1;
}

inline void encodeBoolInInt(int& inout, bool value)
{
    setNthBit(inout, 0, value);
}

inline void decodeBoolFromInt(int in, bool& value)
{
    value = getNthBit(in, 0);
}

inline void encodeCharPart(int& inout, int n, char value)
{
    encodeBoolInInt(inout, getNthBit(value, n));
}

inline void decodeCharPart(int in, int n, char& value)
{
    bool bit;
    decodeBoolFromInt(in, bit);
    setNthBit(value, n, bit);
}

inline void encodeShortPart(int& inout, int n, unsigned short value)
{
    encodeBoolInInt(inout, getNthBit(value, n));
}

inline void decodeShortPart(int in, int n, unsigned short& value)
{
    bool bit;
    decodeBoolFromInt(in, bit);
    setNthBit(value, n, bit);
}

#endif
