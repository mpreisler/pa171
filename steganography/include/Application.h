#ifndef STEGANOGRAPHY_APPLICATION_H_
#define STEGANOGRAPHY_APPLICATION_H_

#include <QApplication>

class MainWindow;


class Application : public QApplication
{
    public:
        Application(int& argc, char** argv);
        virtual ~Application();

    private:
        MainWindow* mMainWindow;
};

#endif
