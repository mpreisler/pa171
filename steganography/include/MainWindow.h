#ifndef STEGANOGRAPHY_MAIN_WINDOW_H_
#define STEGANOGRAPHY_MAIN_WINDOW_H_

#include <QMainWindow>
#include "ui_MainWindow.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget* parent = 0);
        virtual ~MainWindow();

    private slots:
        void loadImageWithoutSecret();
        void loadImageWithSecret();

        void encodeSecret();
        void decodeSecret();

    private:
        /// UI designed in Qt Designer
        Ui_MainWindow mUI;
};

#endif
