#include "MainWindow.h"
#include "BitManipulation.h"

#include <QDesktopWidget>
#include <QFileDialog>
#include <cassert>
//#include <iostream>
#include <random>

typedef std::vector<std::pair<size_t, size_t> > CoordVector;

static CoordVector generateCoords(size_t width, size_t height, size_t length, const CoordVector& reserved, bool spreadOut)
{
    CoordVector ret;
    size_t i = 0;

    if (!spreadOut)
    {
        for (size_t y = 1; y < height - 1; ++y)
        {
            for (size_t x = 1; x < width - 1; ++x)
            {
                if (std::find(reserved.begin(), reserved.end(), std::make_pair(x, y)) != reserved.end())
                    continue;

                ret.push_back(std::make_pair(x, y));
                ++i;

                if (i >= length)
                    return ret;
            }
        }
    }
    else
    {
        std::mt19937 xGenerator;
        xGenerator.seed(666);
        std::mt19937 yGenerator;
        yGenerator.seed(1337);
        std::uniform_int_distribution<size_t> xDistribution(1, width - 1);
        std::uniform_int_distribution<size_t> yDistribution(1, height - 1);

        while (true)
        {
            const size_t x = xDistribution(xGenerator);
            const size_t y = yDistribution(yGenerator);

            if (std::find(reserved.begin(), reserved.end(), std::make_pair(x, y)) != reserved.end())
                continue;

            ret.push_back(std::make_pair(x, y));
            ++i;

            if (i >= length)
                return ret;
        }
    }
}

static void encodePlainSteganography(const QString& secret, const QImage& in, QImage& out, QImage& debug, bool spreadOut)
{
    out = in;
    debug = QImage(in.width(), in.height(), QImage::Format_RGB32);
    debug.fill(0);

    //debug = in;

    assert(secret.length() <= 63);

    CoordVector lengthCoords = generateCoords(in.width(), in.height(), 8, CoordVector(), spreadOut);
    assert(lengthCoords.size() == 8);

    unsigned short length = static_cast<unsigned short>(secret.length());

    for (int i = 0; i < 8; ++i)
    {
        const int x = lengthCoords[i].first;
        const int y = lengthCoords[i].second;

        QColor lbit1 = out.pixel(x, y);
        int blue = lbit1.blue();
        int debugBlue = 0;
        encodeShortPart(blue, i, length);
        encodeShortPart(debugBlue, i, length);
        lbit1.setBlue(blue);
        out.setPixel(x, y, lbit1.rgb());

        debug.setPixel(x, y, qRgb(255, 0, debugBlue));
    }

    if (length == 0)
        return;

    CoordVector secretCoords = generateCoords(in.width(), in.height(), secret.length() * 8, lengthCoords, spreadOut);
    assert(secretCoords.size() == secret.length() * 8);

    QByteArray plainSecret = secret.toAscii();
    const char* plainSecretStr = plainSecret.constData();

    int i = 0;
    for (size_t j = 0; j < secret.length(); ++j)
    {
        for (size_t k = 0; k < 8; ++k) // 8 bits per char
        {
            const int x = secretCoords[i].first;
            const int y = secretCoords[i].second;

            QColor lbit1 = out.pixel(x, y);
            int blue = lbit1.blue();
            int debugBlue = 0;
            encodeCharPart(blue, k, plainSecretStr[j]);
            encodeCharPart(debugBlue, k, plainSecretStr[j]);
            lbit1.setBlue(blue);
            out.setPixel(x, y, lbit1.rgb());

            debug.setPixel(x, y, qRgb(0, 255, debugBlue));

            ++i;
        }
    }
}

static void decodePlainSteganography(QString& secret, const QImage& in, bool spreadOut)
{
    CoordVector lengthCoords = generateCoords(in.width(), in.height(), 8, CoordVector(), spreadOut);
    assert(lengthCoords.size() == 8);

    unsigned short length = 0;

    for (int i = 0; i < 8; ++i)
    {
        const int x = lengthCoords[i].first;
        const int y = lengthCoords[i].second;

        QColor lbit1 = in.pixel(x, y);
        int blue = lbit1.blue();
        decodeShortPart(blue, i, length);
    }

    if (length == 0)
    {
        secret = "";
        return;
    }

    CoordVector secretCoords = generateCoords(in.width(), in.height(), length * 8, lengthCoords, spreadOut);
    assert(secretCoords.size() == length * 8);

    std::string plainSecretStr;

    int i = 0;
    for (size_t j = 0; j < length; ++j)
    {
        char value = '\0';
        for (size_t k = 0; k < 8; ++k) // 8 bits per char
        {
            const int x = secretCoords[i].first;
            const int y = secretCoords[i].second;

            QColor lbit1 = in.pixel(x, y);
            int blue = lbit1.blue();
            decodeCharPart(blue, k, value);

            ++i;
        }

        plainSecretStr += value;
    }

    secret = plainSecretStr.c_str();
}

MainWindow::MainWindow(QWidget* parent):
    QMainWindow(parent)
{
    mUI.setupUi(this);

    QPixmap empty(QSize(512, 512));
    empty.fill(Qt::black);

    mUI.imageWithoutSecret->setText("");
    mUI.imageWithoutSecret->setPixmap(empty);
    mUI.imageWithSecret->setText("");
    mUI.imageWithSecret->setPixmap(empty);

    QObject::connect(
        mUI.loadImageWithoutSecret, SIGNAL(released()),
        this, SLOT(loadImageWithoutSecret())
    );

    QObject::connect(
        mUI.loadImageWithSecret, SIGNAL(released()),
        this, SLOT(loadImageWithSecret())
    );

    QObject::connect(
        mUI.encodeSecret, SIGNAL(released()),
        this, SLOT(encodeSecret())
    );

    QObject::connect(
        mUI.decodeSecret, SIGNAL(released()),
        this, SLOT(decodeSecret())
    );

    // start centered
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
}

MainWindow::~MainWindow()
{}

void MainWindow::loadImageWithoutSecret()
{
    const QString path = QFileDialog::getOpenFileName(this, "Load image without secret", "Images (*.png)");
    QPixmap pixmap;
    pixmap.load(path);
    mUI.imageWithoutSecret->setPixmap(pixmap);
    mUI.imageWithoutSecret->setFixedSize(pixmap.width(), pixmap.height());
}

void MainWindow::loadImageWithSecret()
{
    const QString path = QFileDialog::getOpenFileName(this, "Load image with secret", "Images (*.png)");
    QPixmap pixmap;
    pixmap.load(path);
    mUI.imageWithSecret->setPixmap(pixmap);
    mUI.imageWithSecret->setFixedSize(pixmap.width(), pixmap.height());
}

void MainWindow::encodeSecret()
{
    QImage input = mUI.imageWithoutSecret->pixmap()->toImage();
    QImage output;
    QImage debug;

    if (mUI.plainMode->isChecked())
        encodePlainSteganography(mUI.secret->text(), input, output, debug, false);
    else if (mUI.spreadOutMode->isChecked())
        encodePlainSteganography(mUI.secret->text(), input, output, debug, true);

    if (mUI.debug->checkState() == Qt::Checked)
        mUI.imageWithSecret->setPixmap(QPixmap::fromImage(debug));
    else
        mUI.imageWithSecret->setPixmap(QPixmap::fromImage(output));
}

void MainWindow::decodeSecret()
{
    QImage input = mUI.imageWithSecret->pixmap()->toImage();

    QString secret = "";

    if (mUI.plainMode->isChecked())
        decodePlainSteganography(secret, input, false);
    else if (mUI.spreadOutMode->isChecked())
        decodePlainSteganography(secret, input, true);

    // TODO: Debug?

    mUI.secret->setText(secret);
}
