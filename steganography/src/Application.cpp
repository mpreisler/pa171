#include "Application.h"
#include "MainWindow.h"

Application::Application(int& argc, char** argv):
    QApplication(argc, argv),

    mMainWindow(0)
{
    setApplicationName("Steganography");
    setApplicationVersion("0.0.1");

    mMainWindow = new MainWindow();

    QObject::connect(
        this, SIGNAL(lastWindowClosed()),
        this, SLOT(quit())
    );
    mMainWindow->show();
}

Application::~Application()
{
    delete mMainWindow;
}
