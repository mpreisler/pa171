﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TextIntoImageCoder
{
    public static class TextToImageCoderSpreadOut
    {
        public static Bitmap EncodeTextToImage(Bitmap inputImage, string text, int seed)
        {
            int textLength = text.Count();
            Size sizeOfImage = inputImage.Size;
            Bitmap workingCopyOfInputImage = new Bitmap(inputImage);
            List<int> positionsSequence = GetSequence(seed, sizeOfImage, textLength);
            int positionCounter = 0;

            foreach(char c in text)
            {
                string binaryRepresentationOfChar = ToBin(c, 8);
                foreach(char b in binaryRepresentationOfChar)
                {
                    Point position = GetPosition(positionsSequence.ElementAt(positionCounter), sizeOfImage);
                    CodeBitIntoPixel(workingCopyOfInputImage, position, b);
                    positionCounter++;
                }
            }

            return workingCopyOfInputImage;
        }

        private static List<int> GetSequence(int seed, Size size, int textLength)
        {
            Random rand = new Random(seed);
            ISet<int> numbers = new HashSet<int>();
            int maxNumber = size.Height * size.Width;

            for (int i = 0; i < textLength * 8; i++)
            {
                int nextPosition = rand.Next() + textLength;
                
                if (nextPosition >= maxNumber) 
                    nextPosition = nextPosition % maxNumber;
                if (!numbers.Add(nextPosition)) 
                    i--;
            }
            return new List<int>(numbers);
        }


        private static Point GetPosition(int counter, Size size)
        {
            return new Point(counter % size.Width, counter / size.Width);
        }

        private static string ToBin(int value, int len)
        {
            return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[value & 1];
        }

        private static void CodeBitIntoPixel(Bitmap image, Point position, char bit)
        {
            byte value = image.GetPixel(position.X, position.Y).B;

            if (value % 2 != (int)char.GetNumericValue(bit))
            {
                if (value < 255)
                    value += 1;
                else
                    value -= 1;
                
                image.SetPixel(position.X, position.Y, Color.FromArgb(value, value, value));
                
            }
        }

        public static string DecodeTextFromImage(Bitmap input, int textLength, int seed)
        {
            int positionCounter = 0;
            StringBuilder sb = new StringBuilder("");
            List<int> positionsSequence = GetSequence(seed, input.Size, textLength);

            for (int i = 0; i < textLength; i++)
            {
                sb.Append(getNextChar(input, positionCounter, positionsSequence));
                positionCounter += 8;
            }

            return sb.ToString();
        }


        private static char getNextChar(Bitmap image, int positionCounter, List<int> positionsSequence)
        {
            int result = 0;

            for (int i = 0; i <= 7; i++)
            {
                Point position = GetPosition(positionsSequence.ElementAt(positionCounter), image.Size);
                if (image.GetPixel(position.X, position.Y).B % 2 != 0)
                {
                    result = result + (int)Math.Pow(2, 7 - i);
                }
                positionCounter++;
            }

            return Convert.ToChar(result);
        }
    }
}
