﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextIntoImageCoder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void decodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (InputPictureBox.Image == null || textBox1.Text == "")
                MessageBox.Show("No input image loaded or empty text length!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                int textLength = 1;
                int seed = 0;
                if(Int32.TryParse(textBox1.Text, out textLength) && textLength > 0 && Int32.TryParse(seedtTextBox.Text, out seed) && seed > 0)
                    ContainedTextInfo.Text = TextToImageCoderSpreadOut.DecodeTextFromImage((Bitmap)InputPictureBox.Image, textLength, seed);
                else
                    MessageBox.Show("Text length must be a possitive integer number so as code!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void encodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int seed = 0;
            if (InputPictureBox.Image == null || TextToEncodeTextBox.Text == "" || !Int32.TryParse(seedtTextBox.Text, out seed) || seed <= 0)
                MessageBox.Show("No input image loaded or no text to be encoded or code is not possitive number!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                OutputPictureBox.Image = TextToImageCoderSpreadOut.EncodeTextToImage((Bitmap)InputPictureBox.Image,TextToEncodeTextBox.Text, seed);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog() { Filter = "Bitmap (.bmp)|*.bmp" };
            if (ofd.ShowDialog() == DialogResult.OK) 
            {
                Bitmap inputImage = (Bitmap)Bitmap.FromFile(ofd.FileName);
                InputPictureBox.Image = inputImage;
                OutputPictureBox.Image = null;
            }

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog() { Filter = "Bitmap (.bmp) | *.bmp" };
            if(sfd.ShowDialog() == DialogResult.OK && OutputPictureBox.Image != null)
            {
                OutputPictureBox.Image.Save(sfd.FileName);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OutputPictureBox.Dispose();
            InputPictureBox.Dispose();
            Application.Exit();
        }

        private void TextToEncodeTextBox_TextChanged(object sender, EventArgs e)
        {
            textBox1.Text = TextToEncodeTextBox.Text.Count().ToString();
        }
    }
}
