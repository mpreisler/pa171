﻿namespace TextIntoImageCoder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.imageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.encodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.decodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageInputLabel = new System.Windows.Forms.Label();
            this.InputPictureBox = new System.Windows.Forms.PictureBox();
            this.OutputPictureBox = new System.Windows.Forms.PictureBox();
            this.ImageOutputLabel = new System.Windows.Forms.Label();
            this.TextToEncodeLabel = new System.Windows.Forms.Label();
            this.TextToEncodeTextBox = new System.Windows.Forms.TextBox();
            this.ContainedTextLabel = new System.Windows.Forms.Label();
            this.ContainedTextInfo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.seedLabel = new System.Windows.Forms.Label();
            this.seedtTextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutputPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imageToolStripMenuItem,
            this.encodeToolStripMenuItem,
            this.decodeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(773, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // imageToolStripMenuItem
            // 
            this.imageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.imageToolStripMenuItem.Name = "imageToolStripMenuItem";
            this.imageToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.imageToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.loadToolStripMenuItem.Text = "Load Input";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.saveToolStripMenuItem.Text = "Save Output";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(136, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // encodeToolStripMenuItem
            // 
            this.encodeToolStripMenuItem.Name = "encodeToolStripMenuItem";
            this.encodeToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.encodeToolStripMenuItem.Text = "Encode";
            this.encodeToolStripMenuItem.Click += new System.EventHandler(this.encodeToolStripMenuItem_Click);
            // 
            // decodeToolStripMenuItem
            // 
            this.decodeToolStripMenuItem.Name = "decodeToolStripMenuItem";
            this.decodeToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.decodeToolStripMenuItem.Text = "Decode";
            this.decodeToolStripMenuItem.Click += new System.EventHandler(this.decodeToolStripMenuItem_Click);
            // 
            // ImageInputLabel
            // 
            this.ImageInputLabel.AutoSize = true;
            this.ImageInputLabel.Location = new System.Drawing.Point(13, 148);
            this.ImageInputLabel.Name = "ImageInputLabel";
            this.ImageInputLabel.Size = new System.Drawing.Size(66, 13);
            this.ImageInputLabel.TabIndex = 1;
            this.ImageInputLabel.Text = "Image Input:";
            // 
            // InputPictureBox
            // 
            this.InputPictureBox.Location = new System.Drawing.Point(13, 165);
            this.InputPictureBox.Name = "InputPictureBox";
            this.InputPictureBox.Size = new System.Drawing.Size(352, 275);
            this.InputPictureBox.TabIndex = 2;
            this.InputPictureBox.TabStop = false;
            // 
            // OutputPictureBox
            // 
            this.OutputPictureBox.Location = new System.Drawing.Point(372, 165);
            this.OutputPictureBox.Name = "OutputPictureBox";
            this.OutputPictureBox.Size = new System.Drawing.Size(389, 275);
            this.OutputPictureBox.TabIndex = 3;
            this.OutputPictureBox.TabStop = false;
            // 
            // ImageOutputLabel
            // 
            this.ImageOutputLabel.AutoSize = true;
            this.ImageOutputLabel.Location = new System.Drawing.Point(371, 148);
            this.ImageOutputLabel.Name = "ImageOutputLabel";
            this.ImageOutputLabel.Size = new System.Drawing.Size(72, 13);
            this.ImageOutputLabel.TabIndex = 4;
            this.ImageOutputLabel.Text = "Image output:";
            // 
            // TextToEncodeLabel
            // 
            this.TextToEncodeLabel.AutoSize = true;
            this.TextToEncodeLabel.Location = new System.Drawing.Point(12, 30);
            this.TextToEncodeLabel.Name = "TextToEncodeLabel";
            this.TextToEncodeLabel.Size = new System.Drawing.Size(82, 13);
            this.TextToEncodeLabel.TabIndex = 5;
            this.TextToEncodeLabel.Text = "Text to encode:";
            // 
            // TextToEncodeTextBox
            // 
            this.TextToEncodeTextBox.Location = new System.Drawing.Point(100, 27);
            this.TextToEncodeTextBox.Multiline = true;
            this.TextToEncodeTextBox.Name = "TextToEncodeTextBox";
            this.TextToEncodeTextBox.Size = new System.Drawing.Size(661, 45);
            this.TextToEncodeTextBox.TabIndex = 6;
            this.TextToEncodeTextBox.TextChanged += new System.EventHandler(this.TextToEncodeTextBox_TextChanged);
            // 
            // ContainedTextLabel
            // 
            this.ContainedTextLabel.AutoSize = true;
            this.ContainedTextLabel.Location = new System.Drawing.Point(12, 75);
            this.ContainedTextLabel.Name = "ContainedTextLabel";
            this.ContainedTextLabel.Size = new System.Drawing.Size(78, 13);
            this.ContainedTextLabel.TabIndex = 7;
            this.ContainedTextLabel.Text = "Contained text:";
            // 
            // ContainedTextInfo
            // 
            this.ContainedTextInfo.AutoSize = true;
            this.ContainedTextInfo.Location = new System.Drawing.Point(96, 75);
            this.ContainedTextInfo.Name = "ContainedTextInfo";
            this.ContainedTextInfo.Size = new System.Drawing.Size(27, 13);
            this.ContainedTextInfo.TabIndex = 8;
            this.ContainedTextInfo.Text = "N/A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Text length:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(82, 123);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 10;
            this.textBox1.Text = "1";
            // 
            // seedLabel
            // 
            this.seedLabel.AutoSize = true;
            this.seedLabel.Location = new System.Drawing.Point(188, 126);
            this.seedLabel.Name = "seedLabel";
            this.seedLabel.Size = new System.Drawing.Size(35, 13);
            this.seedLabel.TabIndex = 11;
            this.seedLabel.Text = "Code:";
            // 
            // seedtTextBox
            // 
            this.seedtTextBox.Location = new System.Drawing.Point(229, 123);
            this.seedtTextBox.Name = "seedtTextBox";
            this.seedtTextBox.Size = new System.Drawing.Size(136, 20);
            this.seedtTextBox.TabIndex = 12;
            this.seedtTextBox.Text = "1234";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 452);
            this.Controls.Add(this.seedtTextBox);
            this.Controls.Add(this.seedLabel);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ContainedTextInfo);
            this.Controls.Add(this.ContainedTextLabel);
            this.Controls.Add(this.TextToEncodeTextBox);
            this.Controls.Add(this.TextToEncodeLabel);
            this.Controls.Add(this.ImageOutputLabel);
            this.Controls.Add(this.OutputPictureBox);
            this.Controls.Add(this.InputPictureBox);
            this.Controls.Add(this.ImageInputLabel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InputPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OutputPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem imageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label ImageInputLabel;
        private System.Windows.Forms.PictureBox InputPictureBox;
        private System.Windows.Forms.PictureBox OutputPictureBox;
        private System.Windows.Forms.Label ImageOutputLabel;
        private System.Windows.Forms.Label TextToEncodeLabel;
        private System.Windows.Forms.TextBox TextToEncodeTextBox;
        private System.Windows.Forms.ToolStripMenuItem encodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem decodeToolStripMenuItem;
        private System.Windows.Forms.Label ContainedTextLabel;
        private System.Windows.Forms.Label ContainedTextInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label seedLabel;
        private System.Windows.Forms.TextBox seedtTextBox;
    }
}

